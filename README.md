# Zenoh Computer Vision

This [link](https://github.com/eclipse-zenoh/zenoh-demos/tree/main/computer-vision) takes you to the official GitHub repository of ZettaScale, where you can install and run the `ZettaScale Object Detection Network Application (Zenoh Computer Vision Application)`.